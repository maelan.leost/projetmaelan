#include "list.h"
#include <assert.h>

struct list_t *new_list(){
    struct list_t *NewListe = (struct list_t*)malloc(sizeof(struct list_t)); 
    assert(NewListe); 
    NewListe->head = NULL; 
    NewListe->tail = NULL; 
    NewListe->numelm = 0; 
    return NewListe;}

struct list_t *listcpy(const struct list_t *L){
    struct list_t *copie = new_list(); 
    struct elmlist_t *i = get_head(L); 
    while (i != NULL) {
        queue(copie, i->data);
        i = i->suc; 
    }
    return copie;
}

void del_list(struct list_t **ptrL, void (*ptrf)())
{
    assert(*ptrL); 

    struct elmlist_t *i = (*ptrL)->head; 

    while (i != NULL)
    {
        struct elmlist_t *aSupprimer = i; 
        i = i->suc; 
        del_elmlist(&aSupprimer, ptrf);
    }

    *ptrL = NULL;
}

bool is_empty(const struct list_t *L)
{
    return (L->numelm == 0);
}

struct elmlist_t *get_head(const struct list_t *L)
{
    return L->head;
}

struct elmlist_t *get_tail(const struct list_t *L)
{
    return L->tail;
}

int get_numelm(const struct list_t *L)
{
    return L->numelm;
}

void cons(struct list_t *L, void *data)
{
    struct elmlist_t *nouvelElement = new_elmlist(data); 

    if (is_empty(L)) 
    {
        L->head = nouvelElement;
        L->tail = nouvelElement;
    }
    else 
    {
        nouvelElement->suc = L->head; 
        L->head->pred = nouvelElement; 
        L->head = nouvelElement; 
    }

    L->numelm++; 
}

void queue(struct list_t *L, void *data)
{
    struct elmlist_t *nouvelElement = new_elmlist(data); 
    
    if (is_empty(L)) 
    {
        L->head = nouvelElement;
        L->tail = nouvelElement;
    }
    else 
    {
        nouvelElement->pred = L->tail; 
        L->tail->suc = nouvelElement; 
        L->tail = nouvelElement; 
    }

    L->numelm = L->numelm + 1; 
}

void view_list(struct list_t *L, void (*ptrf)())
{
    assert(L); 

    struct elmlist_t *i = L->head; 

    while (i != NULL)
    {
        view_elmlist(i, *ptrf);
        i = i->suc; 
    }
}


