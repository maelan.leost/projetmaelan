#include "dp_array.h"
#include "global.h"



init_opt_chm(struct states_array_t *states){
    states->OPT = (state_t*)malloc((states->num_obj + 1) * (states->Vmax + 1) * sizeof(state_t)); 
    assert(states->OPT); // Vérification
    states->CHM = (state_t*)malloc((states->num_obj + 1) * (states->Vmax + 1) * sizeof(state_t)); 
    assert(states->CHM); // Vérification

    for (int obj = 1 ; obj <= states->num_obj ; obj++)

        for (int bag = 0 ; bag <= states->Vmax ; bag++){
            int idx = obj * (states->Vmax + 1) + bag;
            states->OPT[idx] = UNDTR; 
            states->CHM[idx] = UNDTR; 
        }
        
    for (int bag = 0 ; bag <= states->Vmax ; bag++)
        states->CHM[bag] = UNDTR; 
}

void free_states_array(struct states_array_t **ptrS){
    assert(*ptrS); 
    if ((*ptrS)->OPT != NULL) 
        free((*ptrS)->OPT);

    if ((*ptrS)->CHM != NULL) 
        free((*ptrS)->CHM);

    free(*ptrS);
    *ptrS = NULL; 
}

struct states_array_t *new_states_array(const int num_objects, const int Vmax){
    struct states_array_t *NS = (struct states_array_t*)malloc(sizeof(struct states_array_t)); 
    assert(NS); 
    NS->num_obj = num_objects; 
    NS->Vmax = Vmax; 
    init_opt_chm(NS); 
    return NS;
}

void push_object_in_array(struct states_array_t *S, struct objects_t *objects, int i)
{


    for (int bag = 0 ; bag < (S->Vmax + 1) ; bag += 1){
        int pred = (i - 1) * (S->Vmax + 1) + bag; 
        int curr = i * (S->Vmax + 1) + bag; 
        int OPT1 = S->OPT[pred];
        S->CHM[curr] = INFTY; 
        if (objects->objects[i - 1].volume <= bag) {
            int pred_without_i = (i - 1) * (S->Vmax + 1) + bag - objects->objects[i - 1].volume; 
            int OPT2 = S->OPT[pred_without_i] + objects->objects[i - 1].utility;
            if (OPT2 > OPT1) 
            { 
                S->OPT[curr] = OPT2;
                S->CHM[curr] = bag - objects->objects[i - 1].volume; 
            }
            else
                S->OPT[curr] = OPT1;
        }
        else
            S->OPT[curr] = OPT1; 
    }
}

void view_path_array(struct states_array_t *states, struct objects_t *set){
    int obj = states->num_obj;
    int vol = states->Vmax;
    int idx = obj * (states->Vmax + 1) + vol;
    bool stop = (obj == 0);

    printf("******\nTotal packaging utility : %d\n******\n", states->OPT[idx]);

    while (!stop) {
        if (states->CHM[idx] != INFTY) { 
            printf("\tobjet #%d(%d, %d)\n", obj, set->objects[obj - 1].volume, set->objects[obj - 1].utility);
            stop = (states->CHM[idx] == 0);
            vol = states->CHM[idx];
        }
        obj -= 1;
        stop = stop || (obj == 0);
        idx = obj * (states->Vmax + 1) + vol;
    }
    printf("\n");
}

void view_opt(const struct states_array_t *states){
    printf("OPT |\t");

	for(int bag = 0 ; bag < (states->Vmax + 1) ; bag++)
        printf("%2d\t", bag);

	printf("\n----|");

	for(int bag = 0 ; bag < (states->Vmax + 1) ; bag++)
        printf("--------");

	printf("\n");

	for (int obj = 0 ; obj < (states->num_obj + 1) ; obj++){
		printf("%3d |\t", obj);

		for(int bag = 0 ; bag < (states->Vmax + 1) ; bag++){
			int idx = obj * (states->Vmax + 1) + bag;

			if (states->OPT[idx] == INFTY)
                printf("INF\t");
			else if (states->OPT[idx] == UNDTR)
                printf("UND\t");
			else
                printf("%2d\t", states->OPT[idx]);
		}

		printf("\n");
	}
    
	printf("\n");
}

void view_chm(const struct states_array_t *states)
{
    printf("CHM |\t");

    for (int bag = 0 ; bag < (states->Vmax + 1) ; bag++)
        printf("%2d\t", bag);

    printf("\n----|");

    for (int bag = 0 ; bag < (states->Vmax + 1) ; bag++)
        printf("--------");

    printf("\n");

    for (int obj = 0 ; obj < (states->num_obj + 1) ; obj++){
        printf("%3d |\t", obj);
        for (int bag = 0 ; bag < (states->Vmax + 1) ; bag++){
            int idx = obj * (states->Vmax + 1) + bag;

            if (states->CHM[idx] == INFTY)
                printf("PRE\t");
            else if (states->CHM[idx] == UNDTR)
                printf("UND\t");
            else
                printf("%2d\t", states->CHM[idx]);
        }
        printf("\n");
    }
    printf("\n");
}

void dp_array(const int Vmax, const struct objects_t *objects){
    struct states_array_t *states = new_states_array(objects->nb_objects, Vmax);
    assert(states != NULL);
    #ifdef _TRACE_
        view_opt(states);
    #endif
    for (int i = 1; i <= objects->nb_objects; i += 1){
        #ifdef _TRACE_
            printf("#%d Object\n", i);
        #endif
        push_object_in_array(states, objects, i);

        #ifdef _TRACE_
            view_opt(states);
            view_chm(states);
        #endif
    }
    view_path_array(states, objects);
    free_states_array(&states);
}