#include "elmlist.h"

struct elmlist_t *new_elmlist(void *data)
{
    struct elmlist_t *NewEl = (struct elmlist_t*)malloc(sizeof(struct elmlist_t)); 
    assert(NewEl);
    NewEl->data = data; 
    NewEl->pred = NULL; 
    NewEl->suc = NULL; 

    return NewEl;
}

void del_elmlist(struct elmlist_t **ptrE, void (*ptrf)())
{
    assert(*ptrE); 

    if ((*ptrf) != NULL)
        (*ptrf)((*ptrE)->data);

    free(*ptrE); 
    *ptrE = NULL; 
}

struct elmlist_t *get_suc(struct elmlist_t *E)
{
    return E->suc;
}

struct elmlist_t *get_pred(struct elmlist_t *E)
{
    return E->pred;
}

void *get_data(struct elmlist_t *E)
{
    return E->data;
}

void set_suc(struct elmlist_t *E, struct elmlist_t *S)
{
    E->suc = S->suc;
}

void set_pred(struct elmlist_t *E, struct elmlist_t *P)
{
    E->pred = P->pred;
}

void set_data(struct elmlist_t * E, void *data)
{
    E->data = data;
}

void view_elmlist(struct elmlist_t *E, void (*ptrf)())
{
    (*ptrf)(E->data);
}