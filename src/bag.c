#include "bag.h"

struct retained_t *new_bag(){
    struct retained_t *NewBag = (struct retained_t*)malloc(sizeof(struct retained_t));
    assert(NewBag); 
    
    NewBag->objects_list = new_list(); 
    NewBag->utilities_sum = 0; 

    return NewBag;
}

void bagcpy(struct retained_t *newbagpack, const struct retained_t *bagpack){
    newbagpack->objects_list = listcpy(bagpack->objects_list); 
    newbagpack->utilities_sum = bagpack->utilities_sum; 
}


void clean_bag(struct retained_t *bagpack){
    struct list_t *aSupprimer = bagpack->objects_list;
    del_list(&aSupprimer, NULL);
    bagpack->objects_list = new_list(); 
    bagpack->utilities_sum = 0; 
}

void free_bag(struct retained_t **ptrB){
    del_list(&(*ptrB)->objects_list, NULL); 
    free(*ptrB); 
    *ptrB = NULL; 
}




void view_bagpack(struct retained_t *bagpack, const char *title){


    void (*ptr_view_fct)(const struct object_t*) = &view_object;
    printf("\n*****************\nVIEW BAGPACKAGING\t%s\n", title);
    view_list(bagpack->objects_list, ptr_view_fct);
    printf("\t\tWith utilities sum = %d\n\n", bagpack->utilities_sum); 
}

void push_object_in_bag(struct retained_t *bagpack, struct object_t *ptr_object){
    cons(bagpack->objects_list, ptr_object); 
    bagpack->utilities_sum = bagpack->utilities_sum + ptr_object->utility; 
}