#ifndef objects_h
#define objects_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>



struct objects_t // TA pour représenter un ensemble d'objets
{
	struct object_t *objects;
	int nb_objects;
	int first_idx;
};

struct object_t // TA pour représenter un objet
{
	int volume;
	int utility;
};

struct objects_t *new_objects(int argc, char **argv, bool utility);
void view_object(struct object_t *object);
void view_objet_set(struct objects_t *set);

#endif