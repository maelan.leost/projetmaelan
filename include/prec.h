#ifndef prec_h
#define prec_h

#include "objects.h"
#include "bag.h"

void prec(const int Vmax, struct objects_t *obj_set, struct retained_t *bag);

#endif
