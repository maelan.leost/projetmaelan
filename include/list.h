#ifndef list_h
#define list_h

#include <stdbool.h>
#include "elmlist.h"

struct list_t
{
  struct elmlist_t *head, *tail; // pointeurs vers le premier et dernier éléments de la liste
  int numelm; // nombre d'éléments de la liste
};

struct list_t *new_list();
struct list_t *listcpy(const struct list_t *L);
void del_list(struct list_t **ptrL, void (*ptrf)());
bool is_empty(const struct list_t *L);
struct elmlist_t *get_head(const struct list_t *L);
struct elmlist_t *get_tail(const struct list_t *L);
int get_numelm(const struct list_t *L);
void cons(struct list_t *L, void *data);
void queue(struct list_t *L, void *data);
void insert_ordered(struct list_t *L, void *data, int (*cmp_ptrf)());
void view_list(struct list_t *L, void (*ptrf)());

#endif