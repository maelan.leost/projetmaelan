#ifndef INFTY
#define INFTY -1 // Etat "infini" pour indiquer que le sac est plein
#endif

#ifndef UNDTR
#define UNDTR -2 // Etat "indéterminé" pour indiquer une valeur inconnue
#endif

#ifndef _TRACE_
#define _TRACE_
#endif

#ifndef _DEBUG_
#define _DEBUG_
#endif