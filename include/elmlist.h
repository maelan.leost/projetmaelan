#ifndef elmlist_h
#define elmlist_h

#include <stdlib.h>
#include <assert.h>

struct elmlist_t
{
    void *data; // pointeur vers le contenu
    struct elmlist_t *suc; // pointeur vers l'élément suivant
    struct elmlist_t *pred; // pointeur vers l'élément précédent
};

struct elmlist_t *new_elmlist(void *data);
void del_elmlist(struct elmlist_t **ptrE, void (*ptrf)());
struct elmlist_t *get_suc(struct elmlist_t *E);
struct elmlist_t *get_pred(struct elmlist_t *E);
void *get_data(struct elmlist_t *E);
void set_suc(struct elmlist_t *E, struct elmlist_t *S);
void set_pred(struct elmlist_t *E, struct elmlist_t *P);
void set_data(struct elmlist_t *E, void *data);
void view_elmlist(struct elmlist_t *E, void (*ptrf)()); 

#endif