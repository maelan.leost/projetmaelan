#ifndef dp_array_h
#define dp_array_h

#include "global.h"
#include "objects.h"

typedef int state_t; 

struct states_array_t
{
    state_t *OPT; 
    state_t *CHM; 
    int num_obj, Vmax;
};



// Créer une matrice d'états ET l'initialiser à "vide"
struct states_array_t *new_states_array(const int num_objects, const int Vmax);

// Visualiser les chemins associés aux états de la matrice d'états
void view_chm(const struct states_array_t *states);

// Ajouter un objet dans les états "valides" de la matrice
void push_object_in_array(struct states_array_t *S, struct objects_t *objects, int i);

// Libérer la mémoire occupée par la matrice d'états
void free_states_array(struct states_array_t **ptrS);

// Visualiser la matrice d'états
void view_opt(const struct states_array_t *states);

// Visualiser la solution optimale
void view_path_array(struct states_array_t *states, struct objects_t *set);

void dp_array(const int Vmax, const struct objects_t *objects);

#endif